#!/usr/bin/env python3

import subprocess
import requests
import json

TOPO = "topology-netconf"
OAM = subprocess.check_output(
    'kubectl -n onap get services/sdnc-oam -o jsonpath="{.spec.clusterIP}:{.spec.ports[?(@.targetPort==8181)].port}"',
    shell=True)
OAM = OAM.decode('utf-8')
USERNAME = "admin"
PASSWORD = "Kp8bJ4SXszM0WXlhak3eHlcse2gAw84vaoGGmJvUy2U"

s = requests.session()
s.auth = (USERNAME, PASSWORD)
t = s.get("http://%s/restconf/operational/network-topology:network-topology/topology/%s/" % (OAM,TOPO))
j = t.json()
#print (j)

summary = dict(
    status_count=0,
    status=dict(
        connecting=dict(count=0,nodes=[]),
        connected=dict(count=0,nodes=[])),
    capability_count=0,
    capability=dict(),
    mount_count=0,
    mount_failed=[])

for node in j.get("topology",[{}])[0].get("node",[]):
    cstatus = None
    if "netconf-node-topology:connection-status" in node:
        summary["status_count"] += 1
        cstatus = node["netconf-node-topology:connection-status"]
        if not cstatus in summary["status"]:
            summary["status"][cstatus] = dict(count=0,nodes=[])
        summary["status"][cstatus]["count"] += 1
        summary["status"][cstatus]["nodes"].append(node["node-id"])
    capabilities = node.get("netconf-node-topology:available-capabilities",{}).get("available-capability",[])
    for cap in capabilities:
        summary["capability_count"] += 1
        cname = cap["capability"]
        if not cname in summary["capability"]:
            summary["capability"][cname] = dict(count=0,nodes=[])
        summary["capability"][cname]["count"] += 1
        summary["capability"][cname]["nodes"].append(node["node-id"])
    if cstatus == "connected":
        r = s.get("http://%s/restconf/operational/network-topology:network-topology/topology/%s/node/%s/yang-ext:mount/" % (OAM,TOPO,node["node-id"]))
        if r.ok:
            summary["mount_count"] += 1
        else:
            summary["mount_failed"].append(node["node-id"])

print(json.dumps(dict(summary=summary,restconf=j)))
